package The_Ast.SympolTyple;

public class Symbol {
    String token;

    @Override
    public String toString() {
        return "Symbol{" +
                "token='" + token + '\'' +
                '}';
    }

    public Symbol(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
