package The_Ast.SympolTyple;

public class Generate_Symbol_Table {
    static SymbolTable symbolTable=new SymbolTable();

    @Override
    public String toString() {
        return "Generate_Symbol_Table{" +
                "symbolTable=" + symbolTable +
                '}';
    }

    public SymbolTable getSymbolTable() {
        return symbolTable;
    }

    public void setSymbolTable(SymbolTable symbolTable) {
        this.symbolTable = symbolTable;
    }

    public void Add_Symbol(String s) {
        Symbol symbol = new Symbol(s);
        getSymbolTable().getScopes().lastElement().getSymbols().add(symbol);
    }

    public void Add_Scope(boolean type) {
        Scope scope = new Scope();
        scope.setPart(type);
        getSymbolTable().getScopes().add(scope);

    }


}
